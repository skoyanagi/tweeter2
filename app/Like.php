<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Tweet;
use App\Like;
use App\User;

class Like extends Model
{
    public function tweet()
    {
        return $this->belongsTo('\App\Tweet');
    }

    public function user()
    {
        return $this->belongsTo('\App\User');
    }

    public static function totalLikeCount($userId)
    {
    	return Like::where('user_id', $userId)->get()->count();
    }

    public static function likeCount($tweetId)
    {
    	return Like::where('tweet_id', $tweetId)->get()->count();
    }

    public static function userLikeCount($tweetId)
    {
    	return Like::where([['tweet_id', '=', $tweetId],['user_id', '=', Auth::user()->id],] )->get()->count();
    }

    public static function getUserLikes($tweetId)
    {
        $likes = Like::where('tweet_id', $tweetId)->get();
        $user = '';
        foreach($likes as $like)
        {
            $userName = User::find($like->user_id)->name;
            $user = $user . $userName . "<br>";            
        }

        return $user;
                
    }

}
