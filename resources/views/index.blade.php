@extends('layouts.marketing')
@section('title')
<h1>Tweeter</h1>
@endsection

<div class="pimg1">
	<div class="ptext">
		<span class="border">Tweeter</span>
	</div>
</div>

<div class="pimg2">
	<div class="ptext">
		<span class="border trans">
			<h1><u>Why should you register a new Account?</u></h1>
		</span>
	</div>
</div>

<section class="section section-dark">
	<div class="section-three jumbotron">
		<div class="row">
			<div class="card item award"
			data-aos="fade-up"
			data-aos-easing="linear"
			data-aos-duration="2500"
			style="width:400px">
			
  			<div class="card-header">
  				<!-- <img class="card-img-top" src="https://livetyping.com/assets/images/blog/bestmobileappawards.png"> -->
  				<img class="card-img-top" src="https://www.bcdtravel.com/wp-content/uploads/bestmobileappawards_platinumaward_h175.png" alt="bestmobileappawards award image.">
    			
  			</div>
	  		<div class="card-body">

	    		<h3 class="card-title font-weight-bold">Voted <strong class="blinking">#1</strong> for the best <em>Tweeter</em> App</h3>
	    		<h3 class="card-title font-weight-bold">in 2018.</h3>
	    		
	  		</div>
		</div>
		</div>

	</div>
</section>

<div class="pimg2">
	<div class="ptext">
		<span class="border trans">
			<h1><u>Happy Tweeter Users</u></h1>
		</span>
	</div>
</div>

<section class="section section-dark">
	<div class="container">
		<div class="row">
	    	<div class="col-lg-4 happy-user-1 happyUsers centered mist"
				data-aos="fade-down"
				data-aos-easing="linear"
				data-aos-duration="1500">
				<img src="https://www.economist.com/sites/default/files/20180120_BLP515.jpg" width="250" alt="donald j. trump happy tweeter user.">
				<div
					data-aos="flip-left"
					data-aos-easing="linear"
					data-aos-duration="2000">
					<h3>Donald J. Trump</h3>	
				</div>
				
				<div
					data-aos="fade-up"
					data-aos-easing="linear"
					data-aos-duration="2000">
					<div>
						<h4>I just love this new Tweeter App!  I just can't stop tweeting.  Let's make AMERICA GREAT AGAIN!</h4>
						<h4>So, what are you waiting for?  Register now!</h4>
					</div>
				</div>
			</div>

			<div class="col-lg-4 happy-user-2 happyUsers centered honey"
				data-aos="fade-down"
				data-aos-easing="linear"
				data-aos-duration="1500">
				<img src="https://www.cp24.com/polopoly_fs/1.4291355.1549889272!/httpImage/image.jpg_gen/derivatives/landscape_620/image.jpg" width="250" alt="ellen DeGeneres happy tweeter user.">
				<div
					data-aos="flip-left"
					data-aos-easing="linear"
					data-aos-duration="2000">
					<h3>Ellen DeGeneres</h3>	
				</div>
				
				<div
					data-aos="fade-up"
					data-aos-easing="linear"
					data-aos-duration="2000">
					<div>
						<h4>I am Ellen DeGeneres and I just love using Tweeter.<br>I would recommend this app to all of my friends and family.</h4>

					</div>
				</div>
			</div>

			<div class="col-lg-4 happy-user-3 happyUsers centered trapperKeeperGreen"
				data-aos="fade-down"
				data-aos-easing="linear"
				data-aos-duration="1500">
				<img src="https://pmcdeadline2.files.wordpress.com/2017/10/jennifer-lopez-3.jpg?w=446&h=299&crop=1" width="250" alt="jennifer lopez happy tweeter user.">
				<div
					data-aos="flip-left"
					data-aos-easing="linear"
					data-aos-duration="2000">
					<h3>Jennifer Lopez</h3>	
				</div>
				
				<div
					data-aos="fade-up"
					data-aos-easing="linear"
					data-aos-duration="2000">
					<div>
						<h4>I use Tweeter everyday.  I give this app 5 stars!</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="pimg2">
	<div class="ptext">
		<span class="border trans">
		<h1><u>New Features</u></h1>
		</span>
	</div>
</div>

<section class="section section-dark">
	<div class="container">
		<div class="row">	
			<div class="featuresSection">
				<div class="col-lg-4 col-md-4 feature-1 newFeatures centered mist"
					data-aos="fade-down"
					data-aos-easing="linear"
					data-aos-duration="1500">
					<img class="wiggle" src="http://we-mobi.com/wp-content/uploads/2014/10/com_mobisystems_android_notifications.png" width="50" alt="image of a bell">
					<div
						data-aos="flip-left"
						data-aos-easing="linear"
						data-aos-duration="2000">
						<h3>Notification System</h3>	
					</div>
			
					<div
						data-aos="fade-up"
						data-aos-easing="linear"
						data-aos-duration="2000">
						<h4>Never miss a notification when someone:</h4>
						<div>
							<ul>
								<li class="text-primary text-left">Likes your tweet!</li>
								<li class="text-primary text-left">Follows you!</li>
								<li class="text-primary text-left">Comments on your tweet!</li>
								<li class="text-primary text-left">Replies to your comment!</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 feature-2 newFeatures centered honey"
					data-aos="fade-down"
	    			data-aos-easing="linear"
	    			data-aos-duration="1500">
	    			<img class="wiggle" src="http://we-mobi.com/wp-content/uploads/2014/10/com_mobisystems_android_notifications.png" width="50" alt="image of a bell">
	    			<div
	    				data-aos="flip-left"
						data-aos-easing="linear"
						data-aos-duration="2000">
	    				<h3>GIF Comment.</h3>
					</div>
					<div
						data-aos="fade-up"
						data-aos-easing="linear"
						data-aos-duration="2000">
						<h4>Have you ever wished that you can express your feelings with a GIF image?</h4>
						<h4>Use the newly integrated <strong>GIF Searching Tool</strong> to find GIF images to use for your comments.</h4>
					</div>
				</div>
		
				<div class="col-lg-4 col-md-4 newFeatures feature-3 centered trapperKeeperGreen"
					data-aos="fade-down"
					data-aos-easing="linear"
					data-aos-duration="1500">
					<img class="wiggle" src="http://we-mobi.com/wp-content/uploads/2014/10/com_mobisystems_android_notifications.png" width="50" alt="image of a bell">
					<div
						data-aos="fade-up"
						data-aos-easing="linear"
						data-aos-duration="2000">
						<h3>Profile Card</h3>
					</div>
					<div
						data-aos="fade-up"
						data-aos-easing="linear"
						data-aos-duration="2000">
						<h4>You asked for it, so we added the Profile Card.</h4>
						<h4>The new user Profile Card allows you to see who just liked your Tweet, or who just followed you.</h4>
						<h4>See user Profile Card when the mouse is hovering over the name.</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
  	

<div class="pimg3">
	<div class="ptext">
  		<span class="border trans">Have You Registered Yet?</span>
	</div>
</div>

<section class="section section-dark">
    <div class="section-three jumbotron">
		<div class="row">
			<div class="itemWaitingFor" id="blue" data-aos="fade-down"
					data-aos-easing="linear"
					data-aos-duration="1500">
				<div 
					data-aos="fade-down"
					data-aos-easing="linear"
					data-aos-duration="1500">
		    		<h2>So, what are you waiting for?</h2>
		    	</div>
	    		<div data-aos="fade-down"
					data-aos-easing="linear"
					data-aos-duration="2000">
					<h2>If you have not registered, register today!</h2>
				</div>
				<div
					data-aos="fade-down"
					data-aos-easing="linear"
					data-aos-duration="2000">
					<h2>
						For a limited time, receive a $25 Amazon Gift-Card just for registering a new Account!
					</h2>	
				</div>
				<br>
				<ul class="list-unstyled">
					<li><a class="blinking" href="{{ url('/register') }}">REGISTER NOW</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="pimg1">
	<div class="ptext">
	  <span class="border">Tweeter</span>
	</div>
</div>