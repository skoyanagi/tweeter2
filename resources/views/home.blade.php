<?php
    $count = 0;
    $profile = "";
    $hasComments = false;
    $setCommentsHeader = false;
    $followCount = 0;
    $followingCount = 0;
    $user_id = Auth::user()->id;
    $userName = Auth::user()->name;
    $profileFilename = Auth::user()->profile_filename;
    $whoToFollow = \App\User::whereRaw('id != ?', $user_id)->get();
    $following = \App\Follow::whereRaw('follower_id = ?', $user_id)->get();   
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name', 'Tweeter') . " | Dashboard" }}</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
  <link
    rel="stylesheet"
    href="https://unpkg.com/tippy.js@4/themes/google.css"
  />
  @include('layouts.links')

    <script>
        $(function () {
              $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    <script>
        tippy('button')
    </script>

  <style type="text/css">
    body {
        margin-top: 0px;
        background-color: #ffff80;
        color: #000;
        font-weight: 400;
    }

    #logout {
        color: white;
        padding: 15px 50px 5px 50px;
        float: right;
        font-size: 16px;
    }
    #tweet {
        color: white;
    }

    .action {
        width: 150px;
    }

    .column {
        float: left;
        width: 15%;
    }

    .column h5 {
        text-align: center;

    }

    .row:after {
        content: "";        
        display: table;
        clear: both;
    }

    th {
        background-color: #1E90FF;
        color: #000;"
    }

    .tweet-table-fixed-height {
        height: 525px;
        overflow: scroll
    }

    .follow-table-fixed-height {
        height: 400px;
        overflow: scroll
    }

    .fa-chevron-circle-right {
        color: #191970;
    }

    .like, .unlike {
        width: 65px;
    }

    .fa-twitter {
        color: #00BFFF;
    }
  </style>

  <!-- =======================================================
    Template Name: Spot
    Template URL: https://templatemag.com/spot-bootstrap-freelance-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>
<body>
    <div id="app">
        <div id="wrapper">

            <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
                
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="">Tweeter</a>
                </div>

                <div id="logout">
                    <a href="{{ url('/logout') }}" class="btn btn-danger square-btn-adjust">Logout</a> 
                </div>
            </nav>

            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        <li class="text-center">                        
                            @if(!is_null($profileFilename))
                                <img src="{{asset('storage/upload/'.$profileFilename)}}" class="user-image img-responsive"/>
                            @else
                                <img src="{{ url('/')}}/img/find_user.png" class="user-image img-responsive"/>
                            @endif
                        </li>
                            
                        <li>
                            <a class="active-menu"  href=""><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                        </li>
                         <li>                        
                            <a href="/user/{{ Auth::user()->id }}/edit"><i class="fa fa-user fa-3x"></i> Edit Profile</a>
                        </li>
                        <li>
                            <a href="#newTweet" data-user_id={{ Auth::user()->id }} data-toggle="modal"><i class="fa fa-twitter fa-3x"></i>New Tweet</a>
                        </li>                       
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper" >
                <div id="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Dashboard</h2>   
                            <h5>Welcome {{ Auth::user()->name }}, Love to see you back! </h5>
                        </div>                                          
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="column">
                                    <h5>Tweets</h5>
                                    <h5>{{ App\User::tweetCount($user_id) }}</h5>
                                </div>
                                <div class="column"
                                    data-tippy="{{ App\User::getListOfFollowers($user_id) }}"
                                    data-tippy-animation="scale"
                                    data-tippy-duration="0"
                                    data-tippy-arrow="true"
                                    allowHTML="true"
                                    animation="shift-away"
                                    data-tippy-delay="[800, 200]">
                                    <h5>Following</h5>
                                    <h5>
                                        {{ App\User::followCount($user_id) }}
                                    </h5>                                
                                </div>
                                <div class="column"
                                    data-tippy="{{ App\User::getListOfPeopleFollowingMe($user_id) }}"
                                    data-tippy-animation="scale"
                                    data-tippy-duration="0"
                                    data-tippy-arrow="true"
                                    allowHTML="true"
                                    animation="shift-away"
                                    data-tippy-delay="[800, 200]">                            
                                    <h5>Followers</h5>                        
                                    <h5>{{ App\User::followersCount($user_id) }}</h5>
                                </div>
                                <div class="column">
                                    <h5>Likes</h5>                        
                                    <h5>{{ App\Like::totalLikeCount($user_id) }}</h5>
                                </div>

                            </div>            
                        </div>
        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header text-white bg-primary">
                                    <h2 id="tweet">Tweets</h2>
                                </div>
                                <div>
                                    {{ $tweets->links() }}
                                </div>
                                <div class="card-body bg-info">
                                    <div class="tweet-table-fixed-height table-responsive">

                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Tweet</th>                                                
                                                    <th>Date</th>
                                                    <th>Likes</th>
                                                    <th>Comments</th>
                                                    <th></th>                                                
                                                </tr>
                                            </thead>
                                            <tbody>

                                              @foreach($tweets as $tweet)

                                                @if($tweet->user_id == Auth::user()->id)
                                                    <?php $count++; ?>
                                                    <tr>
                                                        <td width='50' class='table_width'>{{ $count }}</td>
                                                        <td width='50'><a href="/user/{{ Auth::user()->id }}/edit">
                                                            <img src="{{ asset('storage/upload/'.$tweet->user->profile_filename)}}"
                                                            class="profile-image img-responsive"></a></td>
                                                        @if( strlen($tweet->user->screen_name) == 0 )
                                                            <td width='150' class='table_width'>{{ $tweet->user->name }}</td>
                                                        @else
                                                            <td width='150' class='table_width'>{{ $tweet->user->screen_name }}</td>
                                                        @endif
                                                        <td width='500' class='table_width'>{{ $tweet->tweet_text }}</td>
                                                        <td width='175' class='table_width'>{{ $tweet->created_at }}</td>                                       
                                                        <td width='50' class='table_width'
                                                            data-tippy="{{ App\Like::getUserLikes($tweet->id) }}"
                                                            data-tippy-animation="scale"
                                                            data-tippy-duration="0"
                                                            data-tippy-arrow="true"
                                                            allowHTML="true"
                                                            animation="shift-away"
                                                            data-tippy-delay="[800, 200]">
                                                            <likecount-component
                                                                :tweet-id={{ $tweet->id }}
                                                                :like-count= {{ App\Like::likeCount($tweet->id) }}  
                                                            >
                                                            </likecount-component></td>
                                                        <td width='50' class='table_width'>{{ $tweet->comments->count() }}</td>
                                                        <td width='125'>
                                                            <div class="btn-group">
                                                                <button data-toggle="dropdown" class="action btn btn-primary dropdown-toggle">Action <span class="caret"></span></button>
                                                                <ul class="dropdown-menu">
                                                                    @if(App\Like::userLikeCount($tweet->id) == 0)
                                                                        <like-component
                                                                            :tweet-id="{{ $tweet->id }}"
                                                                        >
                                                                        </like-component>
                                                                    @else
                                                                        <unlike-component
                                                                            :tweet-id="{{ $tweet->id }}"
                                                                        >
                                                                        </unlike-component>
                                                                    @endif
                                                                    @if($tweet->user->id == Auth::user()->id)
                                                                        <li><a href="#editTweet" data-tweetid={{$tweet->id}} data-tweettext="{{ $tweet->tweet_text }}" data-toggle="modal">Edit Tweet</a></li>
                                                                        <li><a href="#deleteTweet" data-tweetid={{$tweet->id}} data-toggle="modal">Delete Tweet</a></li>
                                                                    @endif
                                                                    <li><a href="#retweet" data-tweetid={{$tweet->id}} data-tweettext="{{ $tweet->tweet_text }}" data-tweetusername="{{$tweet->user->name}}" data-userid={{$user_id}} data-toggle="modal">Retweet</a></li>
                                                                    <li class="divider"></li>                                                                
                                                                    <li><a href="{{ route('comment.show',$tweet->id)}}" target="_blank">Show Comments</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @else            
                                                    
                                                    @if(\App\Follow::isFollowing(Auth::user()->id, $tweet->user_id) > 0)
                                                        <?php $count++; ?>
                                                        <tr>
                                                            <td width='50' class='table_width'>{{ $count }}</td>
                                                            <td width='50'><a href=""><img src="{{ asset('storage/upload/'.$tweet->user->profile_filename)}}" class="profile-image img-responsive"></a></td>
                                                            @if( strlen($tweet->user->screen_name) == 0)
                                                                <td width='150' class='table_width'>{{ $tweet->user->name }}</td>
                                                            @else
                                                                <td width='150' class='table_width'>{{ $tweet->user->screen_name }}</td>
                                                            @endif
                                                            <td width='500' class='table_width'>{{ $tweet->tweet_text }}</td>
                                                            <td width='175' class='table_width'>{{ $tweet->created_at }}</td>
                                                            <td width='50' class='table_width' data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="{{ App\Like::getUserLikes($tweet->id) }}">{{ App\Like::likeCount($tweet->id) }}</td>
                                                            <td width='50' class='table_width'>{{ $tweet->comments->count() }}</td>

                                                            
                                                            <td width='125'>
                                                                <div class="btn-group">
                                                                    <button data-toggle="dropdown" class="action btn btn-primary dropdown-toggle">Action <span class="caret"></span></button>
                                                                    <ul class="dropdown-menu">
                                                                        @if(App\Like::userLikeCount($tweet->id) == 0)                                                                            
                                                                            <like-component
                                                                                :tweet-id="{{ $tweet->id }}"
                                                                            >
                                                                            </like-component>
                                                                        @else
                                                                            <unlike-component
                                                                                :tweet-id="{{ $tweet->id }}"
                                                                            >
                                                                            </unlike-component>
                                                                        @endif                                                                    
                                                                        <li><a href="#retweet" data-tweetid={{$tweet->id}} data-tweettext="{{ $tweet->tweet_text }}" data-tweetusername="{{$tweet->user->name}}" data-userid={{$user_id}} data-toggle="modal">Retweet</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="{{ route('comment.show',$tweet->id)}}" target="_blank">Show Comments</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>   
                                                    @endif
                                                @endif
                                                
                                            @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">                    
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header text-white bg-primary">
                                    <h2 id="tweet">You may also like</h2>
                                </div>
                                <div class="follow-table-fixed-height card-body bg-info">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>                                                                                                
                                                    <th width='50' class='table_width'>#</th>
                                                    <th width='50'></th>
                                                    <th>Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($whoToFollow as $follow)
                                                    
                                                    @if(\App\Follow::isFollowing($user_id, $follow->id) == 0)
                                                        <?php $followCount++; ?>       
                                                        <tr>                                                        
                                                            <td>{{ $followCount }}</td>
                                                            <td><a href="{{ url('/follow/'. $follow->id)}}"><img src="{{ asset('storage/upload/'.$follow->profile_filename)}}" class="profile-image img-responsive"></a></td>
                                                            {{ $follow->following_id }}
                                                            <td><a href="{{ url('/follow/'. $follow->id)}}">{{ $follow->name }}</a></td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header text-white bg-primary">
                                    <h2 id="tweet">Following</h2>
                                </div>
                                <div class="follow-table-fixed-height card-body bg-info">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width='50' class='table_width'>#</th>
                                                    <th width='50'></th>
                                                    <th>Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($following as $follow)
                                                    <?php $followingCount++; ?>
                                                    <tr>
                                                        <td>{{ $followingCount }}</td>
                                                        <td><a href="{{ url('/follow/'. $follow->following_id)}}"><img src="{{ asset('storage/upload/'.$follow->userFollowing->profile_filename)}}" class="profile-image img-responsive"></a></td>
                                                        <td><a href="{{ url('/follow/'. $follow->following_id)}}">{{ $follow->userFollowing->name }}</a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>                

                    </div>

                </div>
                
            </div>

        </div>

    </div>

    @include('tweet.modal')
    
   
    @include('layouts.footer')
    
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <!-- <script src="assets/js/jquery-1.10.2.js"></script> -->
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>

    @include('tweet.script')

    <script src="https://unpkg.com/popper.js@1/dist/umd/popper.min.js"></script>
    <script src="https://unpkg.com/tippy.js@4"></script>

    <script>
        function limitText(limitField, limitCount, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>

    <script>
        tippy('.test', {
            content: '<strong>Bolded content</strong>'
        })
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
    
</body>

</html>
