<script>

    $('#newTweet').on('show.bs.modal', function (event) {                
        var button = $(event.relatedTarget)
        var user_id = button.data('user_id');            
        var modal = $(this)
        modal.find('.modal-body #user_id').val(user_id);
    })

    $('#editTweet').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var tweetId = button.data('tweetid');
        var tweetText = button.data('tweettext');
        var stringLen = button.data('tweettext').length;
        var modal = $(this)
        modal.find('.modal-body #tweet_id').val(tweetId);
        modal.find('.modal-body #tweetText').val(tweetText);
        modal.find('.modal-body #countdown').val(280 - stringLen);
    })

    $('#retweet').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var tweetId = button.data('tweetid');
        var tweetText = button.data('tweettext');
        var tweetUserName = button.data('tweetusername');
        var userId = button.data('userid');
        var modal = $(this)
        modal.find('.modal-body #tweet_id').val(tweetId);
        modal.find('.modal-body #user_id').val(userId);
        $(this).parent().find('.modal-body #tweetUserName').text(tweetUserName);
        $(this).parent().find('.modal-body #tweetText').text(tweetText);        
        modal.find('.modal-body #countdown').val(280);
        
    })

    $('#deleteTweet').on('show.bs.modal', function (event) {                
        var tweetId = $(event.relatedTarget).data('tweetid');        
        var modal = $(this)
        modal.find('.modal-body #tweet_id').val(tweetId);
    })
            
</script>