@extends('layouts.marketing')
@section('title')
<h1>Tweeter</h1>
@endsection

@section('carousel-area')
<div>
	<div class="jumbotron section-1">

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		    <!-- Indicators -->
		    <ol class="carousel-indicators">
		    	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    	<li data-target="#myCarousel" data-slide-to="1"></li>
		    	<li data-target="#myCarousel" data-slide-to="2"></li>
		    	<li data-target="#myCarousel" data-slide-to="3"></li>
		    </ol>

		    <!-- Wrapper for slides -->
		    <div class="carousel-inner trapperKeeperGreen" role="listbox">

				<div class="item active">
					<img src="https://png.pngtree.com/thumb_back/fh260/back_pic/04/24/51/235836866b71f0b.jpg" alt="Chania" width="460" height="345">
					<div class="carousel-caption">
						<h3>Chania</h3>
						<p>The atmosphere in Chania has a touch of Florence and Venice.</p>
					</div>
				</div>

		      	<div class="item">
		        	<img src="https://cdn.pixabay.com/photo/2017/02/07/09/02/background-2045380__340.jpg" alt="Chania" width="460" height="345">
		        	<div class="carousel-caption">
		          		<h3>Chania</h3>
		          		<p>The atmosphere in Chania has a touch of Florence and Venice.</p>
		        	</div>
		      	</div>
		    
		    	<div class="item">
		        	<img src="https://2jc18v1irh0441xjkl3blrt4-wpengine.netdna-ssl.com/wp-content/uploads/2018/02/maga-shirt-720.jpg" alt="Flower" width="460" height="345">
		        	<div class="carousel-caption">
		          		<h3>Flowers</h3>
		          		<p>Beautiful flowers in Kolymbari, Crete.</p>
		        	</div>
		      	</div>

		      	<div class="item">
		        	<img src="https://bloximages.chicago2.vip.townnews.com/madison.com/content/tncms/assets/v3/editorial/5/3c/53cece7a-e352-5d75-ac7f-b951697cf4af/5b4fab1ac5a69.image.jpg" alt="Flower" width="460" height="345">
		        	<div class="carousel-caption">
		          		<h3>Flowers</h3>
		          		<p>Beautiful flowers in Kolymbari, Crete.</p>
		        	</div>
		      	</div>
		  
		    </div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>

		<script>
			$(document).ready(function(){
	  			// Activate Carousel
	  			$("#myCarousel").carousel();
	    
	  			// Enable Carousel Indicators
	  			$(".item1").click(function(){
	    		$("#myCarousel").carousel(0);
	  		});
	  
	  		$(".item2").click(function(){
	    		$("#myCarousel").carousel(1);
	  		});

	  		$(".item3").click(function(){
	    		$("#myCarousel").carousel(2);
	  		});

	  		$(".item4").click(function(){
	    		$("#myCarousel").carousel(3);
	  		});
	    
	  		// Enable Carousel Controls
	  		$(".left").click(function(){
	    		$("#myCarousel").carousel("prev");
	  		});

	  		$(".right").click(function(){
	    		$("#myCarousel").carousel("next");
	  		});
			});
		</script>
	</div>
</div>

@endsection

@section('marketing-area')

<div class="section-one jumbotron">
	
	<div class="row">

		<div class="card item award"
			data-aos="fade-up"
			data-aos-easing="linear"
			data-aos-duration="2500"
			style="width:400px">
			
  			<div class="card-header">
  				<!-- <img class="card-img-top" src="https://livetyping.com/assets/images/blog/bestmobileappawards.png"> -->
  				<img class="card-img-top" src="https://www.bcdtravel.com/wp-content/uploads/bestmobileappawards_platinumaward_h175.png">
    			
  			</div>
	  		<div class="card-body">

	    		<h3 class="card-title font-weight-bold">Voted <strong class="blinking">#1</strong> for the best <em>Tweeter</em> App</h3>
	    		<h3 class="card-title font-weight-bold">in 2018.</h3>
	    		<p class="card-text">
	    			
	    		</p>
	    		
	  		</div>
		</div>
	</div>
</div>

<div class="section-two jumbotron">
	<h1><u>New Features</u></h1>
	<div class="row">
		<div class="col-lg-4 col-md-4 newFeatures centered mist"
			data-aos="fade-down"
			data-aos-easing="linear"
			data-aos-duration="1500">
			<img class="wiggle" src="http://we-mobi.com/wp-content/uploads/2014/10/com_mobisystems_android_notifications.png" width="50">
			<div
				data-aos="flip-left"
				data-aos-easing="linear"
				data-aos-duration="2000">
				<h3>Notification System</h3>	
			</div>
			
			<div
				data-aos="fade-up"
				data-aos-easing="linear"
				data-aos-duration="2000">
				<h4>Never miss a notification when someone:</h4>
					<div>
						<ul>
							<li class="text-primary text-left">Likes your tweet!</li>
							<li class="text-primary text-left">Follows you!</li>
							<li class="text-primary text-left">Comments on your tweet!</li>
							<li class="text-primary text-left">Replies to your comment!</li>
						</ul>
					</div>
				
			</div>
		</div>
		<div class="col-lg-4 col-md-4 newFeatures centered honey"
			data-aos="fade-down"
		    data-aos-easing="linear"
		    data-aos-duration="1500">
		    <img class="wiggle" src="http://we-mobi.com/wp-content/uploads/2014/10/com_mobisystems_android_notifications.png" width="50">
		    <div
		    	data-aos="flip-left"
				data-aos-easing="linear"
				data-aos-duration="2000">
		    	<h3>GIF Comment.</h3>
			</div>
			<div
				data-aos="fade-up"
				data-aos-easing="linear"
				data-aos-duration="2000">
				<h4>Have you ever wished that you can express your feelings with
				a GIF image?</h4>
				<h4>Use the newly integrated <strong>GIF Searching Tool</strong> to find GIF images to use for your comments.</h4>
				
			</div>
		</div>

		<div class="col-lg-4 col-md-4 newFeatures centered trapperKeeperGreen"
			data-aos="fade-down"
			data-aos-easing="linear"
			data-aos-duration="1500">
			<img class="wiggle" src="http://we-mobi.com/wp-content/uploads/2014/10/com_mobisystems_android_notifications.png" width="50">
			<div
				data-aos="fade-up"
				data-aos-easing="linear"
				data-aos-duration="2000">
				<h3>Profile Card</h3>
			</div>
			<div
				data-aos="fade-up"
				data-aos-easing="linear"
				data-aos-duration="2000">
				<h4>You asked for it, so we added the Profile Card.</h4>
				<h4>The new user Profile Card allows you to see who just liked your Tweet, or who just followed you.</h4>
				<h4>See user Profile Card when the mouse is hovering over the name.</h4>
			</div>
		</div>
	</div>
</div>

<div class="section-three jumbotron">
	<div class="row">
		<div class="itemWaitingFor" id="blue" data-aos="fade-down"
				data-aos-easing="linear"
				data-aos-duration="1500">
			<div 
				data-aos="fade-down"
				data-aos-easing="linear"
				data-aos-duration="1500">
	    		<h2>So, what are you waiting for?</h2>
	    	</div>
	    	<div data-aos="fade-down"
				data-aos-easing="linear"
				data-aos-duration="2000">
				<h2>If you have not registered, register today!</h2>
			</div>
			<ul class="list-unstyled">
				<li><a class="blinking" href="{{ url('/register') }}">REGISTER NOW</a></li>
			</ul>
		</div>
	</div>
</div>

@endsection