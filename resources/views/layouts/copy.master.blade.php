<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name', 'Tweeter') }}</title>
  @include('layouts.links')
</head>

<body>
  

  @include('layouts.navbar')
  
  <div id="headerwrap">
    
    <div class="container">
                    
        <div class="row centered">
              <div class="col-lg-10 col-lg-offset-1">
                
                @yield('title')
                @yield('login')
                @yield('register')
                @yield('user-profile')
                
              </div>
          </div> 
        
      <div class="row centered">
          
        <div class="col-lg-10 col-lg-offset-1">
            @yield('carousel-area')
        </div>
      </div>      
      <div class="row centered">
        
        <div class="col-lg-10 col-lg-offset-1">
            @yield('marketing-area')
        </div>
      </div>
    </div>
  </div>

<!-- headerwrap -->
  
  @include('layouts.footer')

  <div id="copyrights">
    <div class="container">
      <p>
        &copy; Copyrights <strong>Tweeter</strong>. All Rights Reserved
      </p>      
    </div>
  </div>

  @include('layouts.scripts')

  
</body>
</html>
