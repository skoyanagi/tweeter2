<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="keywords">
<meta content="" name="description">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Raleway:400,300,700,900" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="{{ url('/') }}/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!-- Bootstrap CSS File -->
<link href="{{ url('/') }}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Stylesheet File -->
<link href="{{ url('/') }}/css/custom.css" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="{{ url('/') }}/css/style.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

