<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name', 'Tweeter') }}</title>
  @include('layouts.links')
</head>

<body>
  
  @include('layouts.navbar')
  
  @include('layouts.footer')

  <div id="copyrights">
    <div class="container">
      <p>
        &copy; Copyrights <strong>Tweeter</strong>. All Rights Reserved
      </p>      
    </div>
  </div>

  @include('layouts.scripts')

  <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js">
  </script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js">
  </script>

  
</body>
</html>
