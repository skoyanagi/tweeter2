<script>

    $('#newComment').on('show.bs.modal', function (event) {                
        var button = $(event.relatedTarget);
        var tweetId = button.data('tweetid');
        var userId = button.data('userid');
        var modal = $(this)
        modal.find('.modal-body #tweet_id').val(tweetId);
        modal.find('.modal-body #user_id').val(userId);
    })

    $('#replyComment').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var commentId = button.data('commentid');
        var tweetId = button.data('tweetid');
        var userId = button.data('userid');
        var modal = $(this)
        modal.find('.modal-body #commentId').val(commentId);
        modal.find('.modal-body #tweetId').val(tweetId);
        modal.find('.modal-body #user_id').val(userId);
    })

    $('#editComment').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var commentId = button.data('commentid');
        var commentText = button.data('commenttext');
        var stringLen = button.data('commenttext').length;
        var gifUrl = button.data('gifurl');
        var modal = $(this)
        modal.find('.modal-body #comment_id').val(commentId);
        modal.find('.modal-body #commentText').val(commentText);
        modal.find('.modal-body #countdown').val(280 - stringLen);
    })

    $('#deleteComment').on('show.bs.modal', function (event) {     
        var commentId = $(event.relatedTarget).data('commentid');        
        var modal = $(this)
        modal.find('.modal-body #comment_id').val(commentId);
    })    

</script>