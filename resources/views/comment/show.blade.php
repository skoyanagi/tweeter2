<?php
    $count = 0;
    $user_id = Auth::user()->id;
    $userName = Auth::user()->name;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'Tweeter') . " | Comments" }}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    
    @include('layouts.links')

    <script>
        $(function () {
              $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    <style type="text/css">

        body {
            margin-top: 0px;
            background-color: #ffff80;
            color: #000;
            font-weight: 400;
        }
    
        .action {
            width: 150px;
        }
    
        .comment-table-fixed-height {
            height: 550px;
            overflow: scroll
        }

    </style>

</head>
<body>

    <div id="app">
        <div id="wrapper">

            <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
                    
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="">Tweeter</a>
                </div>
                    
            </nav>

            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        <li class="text-center">                        
                            @if(!is_null($tweet->user->profile_filename))
                                <img src="{{asset('storage/upload/'.Auth::user()->profile_filename)}}" class="user-image img-responsive"/>
                            @else
                                <img src="{{ url('/')}}/img/find_user.png" class="user-image img-responsive"/>
                            @endif
                        </li>
                                                
                        <li>
                            <a href="#newComment" data-userid={{Auth::user()->id}} data-tweetid={{$tweet->id}} data-toggle="modal"><i class="fa fa-comments fa-3x"></i>New Comment</a>                        
                        </li>                       
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper" >
                <div id="page-inner">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header text-white bg-primary">
                                    <h2 id="tweet">Tweet</h2>
                                </div>
                                <div class="card-body bg-info">
                                    <div class="tweet-table-fixed-height table-responsive">

                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>                                                
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Tweet</th>                                                
                                                    <th>Date</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td width='50'><img src="{{ asset('storage/upload/'.$tweet->user->profile_filename)}}" class="profile-image img-responsive"></td>
                                                    @if(strlen($tweet->user->screen_name) == 0)                                                
                                                        <td width='150' class='table_width'>{{ $tweet->user->name }}</td>
                                                    @else
                                                        <td width='150' class='table_width'>{{ $tweet->user->screen_name }}</td>
                                                    @endif
                                                    <td width='500' class='table_width'>{{ $tweet->tweet_text }}</td>
                                                    <td width='175' class='table_width'>{{ $tweet->created_at }}</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header text-white bg-primary">
                                    <h2 id="tweet">Comments</h2>
                                </div>
                                <div class="card-body bg-info">
                                    <div class="comment-table-fixed-height table-responsive">

                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th></th>                                                
                                                    <th>Name</th>                                                
                                                    <th>Comment</th>
                                                    <th>Date</th>                                                
                                                    <th></th>                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($tweet->comments as $comment)
                                                    <?php $count++; ?>
                                                    <tr>
                                                        <td width='50' class='table_width'>{{ $count }}</td>
                                                        <td width='50'>
                                                            <img src="{{ asset('storage/upload/'.$comment->user->profile_filename)}}" class="profile-image img-responsive">
                                                        </td>
                                                        @if(strlen($comment->user->screen_name) == 0)
                                                            <td width='150'>{{ $comment->user->name }}</td>
                                                        @else
                                                            <td width='150'>{{ $comment->user->screen_name }}</td>
                                                        @endif
                                                        <td width='500'>{{ $comment->comment }}</td>
                                                        <td width='125'>{{ $comment->created_at }}</td>
                                                        @if(Auth::user()->id == $comment->user_id)
                                                            <td width='125'>
                                                                <div class="btn-group">                                                                
                                                                    <button data-toggle="dropdown" class="action btn btn-primary dropdown-toggle">Action <span class="caret"></span></button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#editComment" data-commentid={{$comment->id}} data-commenttext="{{$comment->comment}}" data-tweetid={{$tweet->id}} data-toggle="modal">Edit Comment</a></li>
                                                                        @if(is_null($comment->comment_id) || $comment->user_id !== Auth::user()->id )
                                                                            <li><a href="#replyComment" data-commentid={{$comment->id}} data-tweetid={{$tweet->id}} data-userid={{Auth::user()->id}} data-toggle="modal">Reply Comment</a></li>
                                                                        @endif
                                                                        <li><a href="#deleteComment" data-commentid={{$comment->id}} data-toggle="modal">Delete Comment</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        @else
                                                            <td width='125'>
                                                                <div class="btn-group">                                                                
                                                                    <button data-toggle="dropdown" class="action btn btn-primary dropdown-toggle">Action <span class="caret"></span></button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#replyComment" data-commentid={{$comment->id}} data-tweetid={{$tweet->id}} data-userid={{Auth::user()->id}} data-toggle="modal">Reply Comment</a></li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('comment.modal')

        </div>
    </div>    

    

    @include('layouts.footer')

    <script src="{{ url('/') }}/assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{ url('/') }}/assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{ url('/') }}/assets/js/jquery.metisMenu.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="{{ url('/') }}/assets/js/custom.js"></script>

    <script>
        function limitText(limitField, limitCount, limitNum) {
            if (limitField.value.length > limitNum) {
                limitField.value = limitField.value.substring(0, limitNum);
            } else {
                limitCount.value = limitNum - limitField.value.length;
            }
        }
    </script>
    
    <script src="{{ asset('js/app.js') }}"></script>

    @include('comment.script')

    
</body>
</html>
